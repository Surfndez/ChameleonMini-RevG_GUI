# ChameleonMini-RevG-GUI

GUI for ChameleonMini RevG  
 
Here you will get the ChameleonMini-RevG-GUI. 
 
This GUI give you a userfriendly tool to manage all things you ever wanted to do with your ChameleonMini. 
 
## Questions / Ideas / Issues 
If you have any questions, ideas, issues please visit the [Issues Page](https://gitlab.com/Gtpy/ChameleonMini-RevG_GUI/issues) and ask your questions, post ideas or report bugs there. 

## Requirements
- OS: Windows
- Microsoft.NET Framework 4.7.1

## Releases
Here you’ll find the [releases](https://gitlab.com/Gtpy/ChameleonMini-RevG_GUI/releases)

## Wiki
following soon here @ gitlab
 
Donation 
---- 
Feel free to donate - any support is welcome.  
- [paypal.me](https://paypal.me/gtpy)
- If you don't like to donate with money - I am welcome for donations of pentesting-products to extend and support my tests - like some more chameleonMini boards (original, chinese one RevG, RevE, rebooted,..), AVRISP mkII, ... 
 
License 
---- 
GNU GENERAL PUBLIC LICENSE Version 3 for more details see license file in root directory. 

## Screenshots

![Main](Screenshots/Main2.png)
![Main](Screenshots/Identify.png)
![Main](Screenshots/Settings.png)
![Main](Screenshots/Logging.png)
![Main](Screenshots/Serial.png)

## thanks to / inspired by 
- original ChameleonMini Project https://github.com/emsec/ChameleonMini
- iceman's great work for RevE rebooted
- bogiton's great work for RevE rebooted
- kgame great work for optimising Crapto1Sharp

