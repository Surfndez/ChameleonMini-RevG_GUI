/*
    <ChameleonMini-GUI "reloaded" is a userfriendly universial tool for ChameleonMini RevG>
    Copyright (C) <2019>  <gtpy>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact me mailto:csf2@protonmail.ch
*/
using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using static System.Diagnostics.Process;
using System.Management;
using System.Collections.Generic;
using System.Diagnostics;

namespace ChameleonMiniGUI
{
    public partial class frm_main : Form
    {
        private SerialPort _serialport = null;
        private bool isConnected = false;
        private string _curr_serialport = string.Empty;
        private string _deviceIdent;
        private string _firmwVersion;

        public string FirmwareVersion
        {
            get { return _firmwVersion; }
            set
            {
                _firmwVersion = value;
                tx_firmware.Text = _firmwVersion;
            }
        }

        private List<string> AvailableCommands { get; set; }

        public frm_main()
        {
            InitializeComponent();

            AvailableCommands = new List<string>();

            tx_output.SelectionStart = 0;
        }

        private void Frm_main_Load(object sender, EventArgs e)
        {
            //SEARCH Serial PORT FROM CHAM (WAIT ANSWER OF VERSION?)
            OpenChamSerialPort();

            if (_serialport != null && _serialport.IsOpen)
            {
                DeviceConnected();
                ChargingStatus();
                Threshold();
                GetAvailableCommands();
                InitHelp();
            }
        }

        private void Threshold()
        {
            string threshold = SendCommand($"THRESHOLD?").ToString();
            tx_thres.Text = threshold;
        }

        private void GetAvailableCommands()  
        {           
            var result = SendCommand($"HELP").ToString();

            if (string.IsNullOrEmpty(result))
                return;

            // split by comma
            var helpArray = result.Split(',');
            if (!helpArray.Any()) return;

            // Set 
            AvailableCommands.Clear();
            AvailableCommands.AddRange(helpArray);
        }

        private void InitHelp() 
        {
            if (!AvailableCommands.Any())
            {
                tx_SerialHelp.Text = "N/A";
            }
            else
            {
                var txt = string.Empty;
                var nl = Environment.NewLine;
                txt = AvailableCommands.Aggregate(txt, (current, c) => current + $"* {c}{nl}");
                tx_SerialHelp.Text = txt.Replace("*", "\u2022");
            }
        }

        private void ChargingStatus()
        {
            //CHECK CHARGING STATUS

            string charging = SendCommand($"CHARGING?").ToString();

            if (charging.Contains($"TRUE"))
            {
                tx_chrg.BackColor = Color.Green;
                tx_chrg.ForeColor = Color.White;
                tx_chrg.Text = charging;
                tx_chrg.SelectionStart = 0;
            }
            else

                tx_chrg.BackColor = Color.Red;
                tx_chrg.ForeColor = Color.White;
                tx_chrg.Text = charging;
                tx_chrg.SelectionStart = 0;
        }

        private void Frm_main_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Close SerialPort if is still open
            if (_serialport != null && _serialport.IsOpen)
            {
                _serialport.Close();
            }
        }

        private void bt_bootloader_Click(object sender, EventArgs e)
        {
            SendCommandWithoutResult("UPGRADE");

            try
            {
                _serialport.Close();
            }
            catch (Exception) { }

            _serialport = null;

            DeviceDisconnected();
        }

        private void bt_reset_Click(object sender, EventArgs e)
        {
            //Send RESET 
            SendCommandWithoutResult("RESET");
            Application.Restart();
        }

        private void bt_rssirefresh_Click(object sender, EventArgs e)
        {
            //Send RSSI 
            var rssi = SendCommand($"RSSI?");
            tx_rssi.Text = rssi.ToString();
        }

        private void bt_disconnect_Click(object sender, EventArgs e)
        {
            Disconnect();
        }

        private void bt_connect_Click(object sender, EventArgs e)
        {
            if (isConnected) return;

            //Try connect

            OpenChamSerialPort();

            if (_serialport != null && _serialport.IsOpen)
            {
                DeviceConnected();
                ChargingStatus();
                Threshold();
            }
            else
            {
                MessageBox.Show("Unable to connect to the Chameleon device", "Connection failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            GetAvailableCommands();
            InitHelp();
        }

        private void DisplayText()
        {
            var ident = string.Empty;
            if (!string.IsNullOrWhiteSpace(_deviceIdent))
            {
                ident = $"({_deviceIdent})";
            }
            this.Text = $"Connected {_curr_serialport} {ident} - {Environment.OSVersion.VersionString}";
        }

        private void DeviceDisconnected()
        {
            if (!isConnected) return;

            isConnected = false;
            try
            {
                _serialport.Close();
            }
            catch (Exception)
            {

            }
            finally
            {
                _serialport = null;
            }

            this.Text = "Device disconnected";

            tx_constatus.Text = "NOT CONNECTED";
            tx_constatus.BackColor = System.Drawing.Color.Red;
            tx_constatus.ForeColor = System.Drawing.Color.White;
            tx_constatus.SelectionStart = 0;

            //Disable controls

            //Button
            //bt_reset.Enabled = false;
            //bt_bootloader.Enabled = false;
            bt_rssirefresh.Enabled = false;
            bt_disconnect.Enabled = false;
            //bt_clear_slot.Enabled = false;
            //bt_change_uid.Enabled = false;
            //bt_clone.Enabled = false;
            //bt_refresh.Enabled = false;
            //bt_read_setting.Enabled = false;
            //bt_write_setting.Enabled = false;
            //bt_read_uid.Enabled = false;
            //bt_identify.Enabled = false;
            //bt_ultralight_read.Enabled = false;
            //bt_show_logmode.Enabled = false;
            //bt_logmem.Enabled = false;
            //bt_logclear.Enabled = false;
            //bt_sniff.Enabled = false;
            //bt_upload.Enabled = false;
            //bt_download.Enabled = false;
            //bt_clearLog.Enabled = false;
            //bt_clear_identify_log.Enabled = false;

            //Tab control
            tabControl1.Enabled = false;

            //Textbox
            //tx_uid.Enabled = false;                 
            tx_constatus.Enabled = false;
            tx_firmware.Enabled = false;
            tx_rssi.Enabled = false;
            tx_chrg.Enabled = false;
            tx_thres.Enabled = false;
            tx_output.Enabled = false;
            //tx_conf_show.Enabled = false;
            //tx_size.Enabled = false;

            //ComboBox
            //cb_slots.Enabled = false;
            //cb_config_new.Enabled = false;
            //cb_led_green.Enabled = false;
            //cb_led_red.Enabled = false;
            //cb_r_btn.Enabled = false;
            //cb_r_btn_long.Enabled = false;
            //cb_l_btn.Enabled = false;
            //cb_l_btn_long.Enabled = false;
            //cb_log_mode.Enabled = false;

            // tab Serial
            //bt_SerialCmd.Enabled = false;
            //tx_serialCmd.Enabled = false;
            tx_SerialHelp.Text = "N/A";

            //only enabled
            bt_connect.Enabled = true;
        }

        private void DeviceConnected()
        {
            if (isConnected) return;

            this.Cursor = Cursors.WaitCursor;

            isConnected = true;

            DisplayText();

            tx_constatus.Text = "CONNECTED!";
            tx_constatus.BackColor = System.Drawing.Color.Green;
            tx_constatus.ForeColor = System.Drawing.Color.White;
            tx_constatus.SelectionLength = 0;

            //Enable controls

            //Tab control
            tabControl1.Enabled = true;

            //Button
            //bt_reset.Enabled = true;
            //bt_bootloader.Enabled = true;
            bt_rssirefresh.Enabled = true;
            bt_disconnect.Enabled = true;
            //bt_clear_slot.Enabled = true;
            //bt_change_uid.Enabled = true;
            //bt_clone.Enabled = true;
            //bt_refresh.Enabled = true;
            //bt_read_setting.Enabled = true;
            //bt_write_setting.Enabled = true;
            //bt_read_uid.Enabled = true;
            //bt_identify.Enabled = true;
            //bt_ultralight_read.Enabled = true;
            //bt_show_logmode.Enabled = true;
            //bt_logmem.Enabled = true;
            //bt_logclear.Enabled = true;
            //bt_sniff.Enabled = true;
            //bt_upload.Enabled = true;
            //bt_download.Enabled = true;
            //bt_clearLog.Enabled = true;
            //bt_clear_identify_log.Enabled = true;

            //Textbox
            //tx_uid.Enabled = true;
            tx_constatus.Enabled = true;
            tx_firmware.Enabled = true;
            tx_rssi.Enabled = true;
            tx_chrg.Enabled = true;
            tx_thres.Enabled = true;
            tx_output.Enabled = true;
            //tx_conf_show.Enabled = true;
            //tx_size.Enabled = true;

            //ComboBox
            //cb_slots.Enabled = true;
            //cb_config_new.Enabled = true;
            //cb_led_green.Enabled = true;
            //cb_led_red.Enabled = true;
            //cb_r_btn.Enabled = true;
            //cb_r_btn_long.Enabled = true;
            //cb_l_btn.Enabled = true;
            //cb_l_btn_long.Enabled = true;
            //cb_log_mode.Enabled = true;

            //only enabled
            bt_connect.Enabled = false;

            this.Cursor = Cursors.Default;
        }

        private void Disconnect()
        {
            if (!isConnected) return;

            if (_serialport != null && _serialport.IsOpen)
            {
                _serialport.Close();
                _serialport = null;
                DeviceDisconnected();

                //clear tx_firmware.Text
                tx_firmware.Clear();
                tx_thres.Clear();
                ChargingStatus();
                tx_output.Text = $"[+] Success{Environment.NewLine}Device disconnected{Environment.NewLine}";
                this.Cursor = Cursors.Default;
            }
        }

        private void OpenChamSerialPort()
        {
            this.Cursor = Cursors.WaitCursor;
            tx_output.Text = string.Empty;

            var searcher = new ManagementObjectSearcher("select Name,DeviceID from Win32_SerialPort ");
            foreach (var obj in searcher.Get())
            {
                var comPortStr = obj["DeviceID"].ToString();

                _serialport = new SerialPort(comPortStr, 115200)
                {
                    ReadTimeout = 4000,
                    WriteTimeout = 6000
                };

                try
                {
                    _serialport.Open();
                    var name = obj["Name"].ToString();
                    tx_output.Text += $"Connecting to {name} at {comPortStr}{Environment.NewLine}";
                }
                catch (Exception)
                {
                    tx_output.Text = $"Failed {comPortStr}{Environment.NewLine}";
                }

                if (_serialport.IsOpen)
                {
                    // version
                    FirmwareVersion = SendCommand("VERSION?") as string;
                    if (!string.IsNullOrEmpty(_firmwVersion) && _firmwVersion.Contains("Chameleon"))
                    {
                        _deviceIdent = "original or modified Firmware ";
                        tx_output.Text = $"[+] Success{Environment.NewLine}Found ChameleonMini device on {comPortStr}{Environment.NewLine}";
                        _curr_serialport = comPortStr;
                        this.Cursor = Cursors.Default;
                        return;
                    }

                    // wrong comport
                    _serialport.Close();
                    tx_output.Text += $"Didn't find ChameleonMini on {comPortStr}{Environment.NewLine}";
                }
            }
            _curr_serialport = string.Empty;
            this.Cursor = Cursors.Default;
            tx_output.Text += $"[!] Didn't find ChameleonMini device connected{Environment.NewLine}";
        }

        //Send command WO/res
        private void SendCommandWithoutResult(string cmdText)
        {
            if (string.IsNullOrWhiteSpace(cmdText)) return;
            if (_serialport == null || !_serialport.IsOpen) return;

            try
            {
                // send command
                var tx_data = Encoding.ASCII.GetBytes(cmdText);
                _serialport.Write(tx_data, 0, tx_data.Length);
                _serialport.Write("\r\n");
            }
            catch (Exception)
            { }
        }

        //Send command W/res                
        private object SendCommand(string cmdText)
        {
            if (string.IsNullOrWhiteSpace(cmdText)) return string.Empty;
            if (_serialport == null || !_serialport.IsOpen) return string.Empty;

            try
            {
                //send command
                var tx_data = Encoding.ASCII.GetBytes(cmdText);
                _serialport.Write(tx_data, 0, tx_data.Length);
                _serialport.Write("\r\n");

                //wait to make sure data is transmitted
                Thread.Sleep(100); //200 RG

                var rx_data = new byte[500]; //orig revE 275 // 500 RG

                //MAYBE later for ADDing some rebooted functions -- read the result
                var read_count = _serialport.Read(rx_data, 0, rx_data.Length);
                if (read_count <= 0) return string.Empty;

                if (cmdText.Contains("DETECTION?"))
                {
                    var foo = new byte[read_count];
                    Array.Copy(rx_data, 8, foo, 0, read_count - 7);
                    return foo;
                }
                else
                {
                    var s = new string(Encoding.ASCII.GetChars(rx_data, 0, read_count));
                    return s.Replace("101:OK WITH TEXT", "").Replace("100:OK", "").Replace("\r\n", "");
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        //added some functions from RevE Rebooted GUI - https://github.com/iceman1001/ChameleonMini-rebooted
        private async Task<object> SendCommand_GTPY(string cmdText)  
        {
            if (string.IsNullOrWhiteSpace(cmdText)) return string.Empty;
            if (_serialport == null || !_serialport.IsOpen) return string.Empty;

            try
            {
                // send command
                var tx_data = Encoding.ASCII.GetBytes(cmdText);
                _serialport.Write(tx_data, 0, tx_data.Length);
                _serialport.Write("\r\n");

                // wait to make sure data is transmitted
                Thread.Sleep(100);

                int blockLimit = 275;  //275

                var cts = new CancellationTokenSource();
                var rx_data = new byte[blockLimit];

                var bytesread = await _serialport.BaseStream.ReadAsync(rx_data, 0, blockLimit, cts.Token);

                if (bytesread <= 0) return string.Empty;


                var received = new byte[bytesread];
                Buffer.BlockCopy(rx_data, 0, received, 0, bytesread);

                if (cmdText.Contains("DETECTION?"))
                {
                    var foo = new byte[bytesread];
                    Array.Copy(rx_data, 8, foo, 0, bytesread - 7);
                    //return foo;
                    var str = BitConverter.ToString(foo).Replace("-", " ");

                    return $"{Environment.NewLine}{str}{Environment.NewLine}";
                }

                // Check if chameleon wants to start XMODEM-Transfer
                var s = new string(Encoding.ASCII.GetChars(received));
                return s;
            }
            catch (Exception ex)
            {
                var msg = $"{Environment.NewLine}[!] {ex.Message}{Environment.NewLine}";
                tx_output.Text += msg;
                return string.Empty;
            }

        }

        #region DUMP LOAD/SAVE
        //LOAD/SAVE DUMP FUNCTIONALITY

        private static byte[] ReadFileIntoByteArray(string filename)
        {
            if (File.Exists(filename))
                return File.ReadAllBytes(filename);
            return null;
        }

        //LOAD DUMP
        internal void LoadDump(string filename)
        {
            //LOAD file into memory block
            var bytes = ReadFileIntoByteArray(filename);

            //Set up XMODEM object
            var xmodem = new XMODEM(_serialport, XMODEM.Variants.XModemChecksum);

            //Send the UPLOAD command
            SendCommandWithoutResult($"UPLOAD");
            _serialport.ReadLine(); //"110:WAITING FOR XMODEM"

            int numBytesSuccessfullySent = xmodem.Send(bytes);

            if (numBytesSuccessfullySent == bytes.Length &&
                xmodem.TerminationReason == XMODEM.TerminationReasonEnum.EndOfFile)
            {
                var msg = $"[+] File upload ok{Environment.NewLine}";
                tx_output.Text += msg;
            }
            else
            {
                var msg = $"[!] Failed to upload file{Environment.NewLine}";
                MessageBox.Show(msg);
                tx_output.Text += msg;
            }
        }

        //SAVE DUMP
        internal void SaveDump(string filename)
        {
            //Set up XMODEM object
            var xmodem = new XMODEM(_serialport, XMODEM.Variants.XModemChecksum);

            //Get the current memsize of the slot
            var memsizeStr = SendCommand($"MEMSIZE?");

            int memsize = 4096; //Def. value

            if (!string.IsNullOrEmpty((string)memsizeStr))
            {
                int.TryParse((string)memsizeStr, out memsize);
            }

            //Check if tag is MF_UL to save counters
            if ((SendCommand($"CONFIG?") is string configStr) && (configStr.Contains("ULTRALIGHT")))
            {
                if (memsize < 4069)
                {
                    memsize += 3 * 4; //3 more pages
                }
            }

            //Send download command
            SendCommandWithoutResult($"DOWNLOAD");

            //"110:WAITING FOR XMODEM" text
            _serialport.ReadLine();

            var ms = new MemoryStream();
            var reason = xmodem.Receive(ms);

            if (reason == XMODEM.TerminationReasonEnum.EndOfFile)
            {
                var msg = $"[+] File download from device ok{Environment.NewLine}";
                tx_output.Text += msg;

                //Transfer successful convert MemStream to byte array
                var bytes = ms.ToArray();

                //Strip away the SUB (byte value 26) padding bytes
                bytes = xmodem.TrimPaddingBytesFromEnd(bytes);

                byte[] neededBytes = bytes;

                if (bytes.Length > memsize)
                {
                    //Create a new array same size as memsize
                    neededBytes = new byte[memsize];

                    Array.Copy(bytes, neededBytes, neededBytes.Length);
                }

                //Write the actual file
                File.WriteAllBytes(filename, neededBytes);

                msg = $"[+] File saved to {filename}{Environment.NewLine}";
                tx_output.Text += msg;
            }
            else
            {
                //If something goes wrong during file transfer
                var msg = $"[!] Failed to save dump{Environment.NewLine}";
                MessageBox.Show(msg);
                tx_output.Text += msg;
            }
        }

        private void bt_upload_Click(object sender, EventArgs e)
        {
            //Open dlg
            openFileDialog1.Filter = "Dump Files|*.mfd" +
                                     "|All Files|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var dumpFilename = openFileDialog1.FileName;

                //Load dump
                LoadDump(dumpFilename);
            }
        }

        private void bt_download_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Dump Files|*.mfd" +
                                         "|All Files|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var dumpFilename = saveFileDialog1.FileName;

                //Save dump
                SaveDump(dumpFilename);
            }
        }
        #endregion

        private void cb_slots_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CYCLE_SETTING - CHANGE SLOT ITEM
            SendCommandWithoutResult($"SETTING=" + (cb_slots.SelectedItem));

            //UID? -> RETURNS THE UID
            var slotUid = SendCommand($"UID?").ToString();
            tx_uid.Text = slotUid;

            //CONFIG? -> RETURNS THE CURRENT CONFIGURATION
            var config = SendCommand($"CONFIG?").ToString();
            tx_conf_show.Text = config;

            //UIDSIZE? -> RETURNS THE UIDSIZE
            var uidSize = SendCommand($"UIDSIZE?").ToString();
            tx_size.Text = uidSize;

        }

        private void bt_clear_identify_log_Click(object sender, EventArgs e)
        {
            tx_identify.Clear();
        }

        private void bt_clear_slot_Click(object sender, EventArgs e)
        {
            // CLEAR
            SendCommandWithoutResult($"CLEAR");

            Thread.Sleep(100);

            //Set Slot to configuration NONE
            SendCommandWithoutResult($"CONFIG=NONE");

            //CONFIG? -> RETURNS THE CURRENT CONFIGURATION
            var config = SendCommand($"CONFIG?").ToString();
            tx_conf_show.Text = config;

            //UIDSIZE? -> RETURNS THE UIDSIZE
            var uidSize = SendCommand($"UIDSIZE?").ToString();
            tx_size.Text = uidSize;

            //UID? -> RETURNS THE UID
            var slotUid = SendCommand($"UID?").ToString();
            tx_uid.Text = slotUid;
        }

        private void bt_change_uid_Click(object sender, EventArgs e)
        {
            //read uid in tx_uid and send command UID={uidvalue} & check if uid matches actual configuration
            string uidvalue = tx_uid.Text;
            string actualconf = tx_conf_show.Text;

            // if mode is classic then UID must be 4 bytes (8 hex digits) long
            if (tx_conf_show.Text.EndsWith("MF_CLASSIC_4K") && tx_uid.Text.Length == 8)
            {
                SendCommandWithoutResult($"UID={uidvalue}");
                //give success in output
                tx_output.Text = $"[+] Success{Environment.NewLine}UID changed to {uidvalue}{Environment.NewLine}";
                this.Cursor = Cursors.Default;
            }

            else if (tx_conf_show.Text.EndsWith("MF_CLASSIC_1K") && tx_uid.Text.Length == 8)
            {
                SendCommandWithoutResult($"UID={uidvalue}");
                //give success in output
                tx_output.Text = $"[+] Success{Environment.NewLine}UID changed to {uidvalue}{Environment.NewLine}";
                this.Cursor = Cursors.Default;
                //TODO: maybe switch tx_output to default
            }

            // if mode is classic then UID must be 7 bytes (14 hex digits) long
            else if (tx_conf_show.Text.EndsWith("MF_CLASSIC_1K_7B") && tx_uid.Text.Length == 14)
            {
                SendCommandWithoutResult($"UID={uidvalue}");
                //give success in output
                tx_output.Text = $"[+] Success{Environment.NewLine}UID changed to {uidvalue}{Environment.NewLine}";
                this.Cursor = Cursors.Default;
            }

            else if (tx_conf_show.Text.EndsWith("MF_CLASSIC_4K_7B") && tx_uid.Text.Length == 14)
            {
                SendCommandWithoutResult($"UID={uidvalue}");
                //give success in output
                tx_output.Text = $"[+] Success{Environment.NewLine}UID changed to {uidvalue}{Environment.NewLine}";
                this.Cursor = Cursors.Default;
            }

            else if (tx_conf_show.Text.Contains("MF_ULTRALIGHT") && tx_uid.Text.Length == 14)
            {
                SendCommandWithoutResult($"UID={uidvalue}");
                //give success in output
                tx_output.Text = $"[+] Success{Environment.NewLine}UID changed to {uidvalue}{Environment.NewLine}";
                this.Cursor = Cursors.Default;
            }
            else
            {
                //give failure in output
                tx_output.Text = $"[!] Failed{Environment.NewLine}UID not changed.{Environment.NewLine}";
                tx_output.Text += $"Check length of UID{Environment.NewLine}";
                this.Cursor = Cursors.Default;
            }
        }

        private void cb_config_new_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CONFIG=cb_config_new.SelectedItem
            SendCommandWithoutResult($"CONFIG=" + (cb_config_new.SelectedItem));

            //CONFIG? -> RETURNS THE CURRENT CONFIGURATION
            var config = SendCommand($"CONFIG?").ToString();
            tx_conf_show.Text = config;

            //UIDSIZE? -> RETURNS THE UIDSIZE
            var uidSize = SendCommand($"UIDSIZE?").ToString();
            tx_size.Text = uidSize;

            //UID? -> RETURNS THE UID
            var slotUid = SendCommand($"UID?").ToString();
            tx_uid.Text = slotUid;

            //give success in output
            tx_output.Text = $"[+] Success{Environment.NewLine}Configuration changed to {config}{Environment.NewLine}";
            this.Cursor = Cursors.Default;
        }

        private void bt_read_setting_Click(object sender, EventArgs e)
        {
            //Read ledgreen and set cb_led_green to value
            var ledgreen = SendCommand($"LEDGREEN?").ToString();
            int indexgreen = cb_led_green.FindString(ledgreen);
            cb_led_green.SelectedIndex = indexgreen;

            //Read ledred and set cb_led_red to value
            var ledred = SendCommand($"LEDRED?").ToString();
            int indexred = cb_led_red.FindString(ledred);
            cb_led_red.SelectedIndex = indexred;

            //Read rbutton and set cb_r_btn to value
            var rbtn = SendCommand($"RBUTTON?").ToString();
            int indexrbtn = cb_r_btn.FindString(rbtn);
            cb_r_btn.SelectedIndex = indexrbtn;

            //Read lbutton and set cb_l_btn to value
            var lbtn = SendCommand($"LBUTTON?").ToString();
            int indexlbtn = cb_l_btn.FindString(lbtn);
            cb_l_btn.SelectedIndex = indexlbtn;

            //Read Rbutton_long and set cb_r_btn_long to value
            var rbtnlong = SendCommand($"RBUTTON_LONG?").ToString();
            int indexrbtnlong = cb_r_btn_long.FindString(rbtnlong);
            cb_r_btn_long.SelectedIndex = indexrbtnlong;

            //Read Lbutton_long and set cb_l_btn_long to value
            var lbtnlong = SendCommand($"LBUTTON_LONG?").ToString();
            int indexlbtnlong = cb_l_btn_long.FindString(lbtnlong);
            cb_l_btn_long.SelectedIndex = indexlbtnlong;

            //give success in output
            tx_output.Text = $"[+] Success{Environment.NewLine}Read all settings done {Environment.NewLine}";
            this.Cursor = Cursors.Default;
        }

        private void bt_write_setting_Click(object sender, EventArgs e)
        {
            //Write Settings =cb_xy.SelectedItem
            SendCommandWithoutResult($"LEDGREEN=" + (cb_led_green.SelectedItem));
            SendCommandWithoutResult($"LEDRED=" + (cb_led_red.SelectedItem));
            SendCommandWithoutResult($"RBUTTON=" + (cb_r_btn.SelectedItem));
            SendCommandWithoutResult($"LBUTTON=" + (cb_l_btn.SelectedItem));
            SendCommandWithoutResult($"RBUTTON_LONG=" + (cb_r_btn_long.SelectedItem));
            SendCommandWithoutResult($"LBUTTON_LONG=" + (cb_l_btn_long.SelectedItem));

            //give success in output
            tx_output.Text = $"[+] Success{Environment.NewLine}Write settings done {Environment.NewLine}";
            this.Cursor = Cursors.Default;
        }

        private void bt_refresh_Click(object sender, EventArgs e)
        {
            //UID? -> RETURNS THE UID
            var slotUid = SendCommand($"UID?").ToString();
            tx_uid.Text = slotUid;

            //CONFIG? -> RETURNS THE CURRENT CONFIGURATION
            var config = SendCommand($"CONFIG?").ToString();
            tx_conf_show.Text = config;

            //UIDSIZE? -> RETURNS THE UIDSIZE
            var uidSize = SendCommand($"UIDSIZE?").ToString();
            tx_size.Text = uidSize;
        }

        private void bt_read_uid_Click(object sender, EventArgs e)
        {
            //Change to slot 8
            SendCommandWithoutResult($"SETTING=8");

            //CONFIG as READER
            SendCommandWithoutResult($"CONFIG=ISO14443A_READER");

            //UID? -> RETURNS THE UID
            var slotUid = SendCommand($"GETUID").ToString();
            tx_identify.Text += $"{Environment.NewLine}";
            tx_identify.Text += ($"UID is: {slotUid}");
            tx_identify.Text += $"{Environment.NewLine}";
        }

        private void bt_identify_Click(object sender, EventArgs e)
        {
            //Change to slot 8
            SendCommandWithoutResult($"SETTING=8");

            //CONFIG as READER
            SendCommandWithoutResult($"CONFIG=ISO14443A_READER");

            //Identify Card
            var identify = SendCommand($"IDENTIFY").ToString();

            tx_identify.Text += "\r\n--Card Identification--\r\n" + identify
                                 .Replace("ATQA:", "\r\nATQA:\t")
                                 .Replace("UID:", "\r\nUID:\t")
                                 .Replace("SAK:", "\r\nSAK:\t");
            tx_identify.Text += $"{Environment.NewLine}";
        }

        private void bt_clone_Click(object sender, EventArgs e)
        {
            //CONFIG as READER
            SendCommandWithoutResult($"CONFIG=ISO14443A_READER");

            //CLONE UID and SWITCH to correct SETTING
            SendCommandWithoutResult($"CLONE");

            //CONFIG? -> first try to check config
            var config = SendCommand($"CONFIG?").ToString();
            tx_conf_show.Text = config;

            //CONFIG? -> RETURNS THE CURRENT CONFIGURATION
            var config_2nd = SendCommand($"CONFIG?").ToString();
            tx_conf_show.Text = config_2nd;

            //UIDSIZE? -> RETURNS THE UIDSIZE
            var uidSize = SendCommand($"UIDSIZE?").ToString();
            tx_size.Text = uidSize;

            //UID? -> RETURNS THE UID
            var slotUid = SendCommand($"UID?").ToString();
            tx_uid.Text = slotUid;

            //Run again - cloning process needs some time...
            var config_3rd = SendCommand($"CONFIG?").ToString();
            tx_conf_show.Text = config_3rd;

            var uidSizeagain = SendCommand($"UIDSIZE?").ToString();
            tx_size.Text = uidSizeagain;

            var slotUidagain = SendCommand($"UID?").ToString();
            tx_uid.Text = slotUidagain;

            //CLEAR OUTPUT
            tx_output.Clear();

            ////maybe give success in output
            //tx_output.Text = $"Success{Environment.NewLine}Clone process is done. {Environment.NewLine}";
            //this.Cursor = Cursors.Default;
        }

        private void bt_ultralight_read_Click(object sender, EventArgs e)
        {
            //DUMP_MFU - Dumps the complete content of a MFU Card

            //CONFIG as READER
            SendCommandWithoutResult($"CONFIG=ISO14443A_READER");

            //DUMP COMMAND
            var dumpMfu = SendCommand($"DUMP_MFU").ToString();

            tx_identify.Text += "\r\n--Dump MFU:--\r\n";

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dumpMfu.Length; i++)
            {
                if (i % 32 == 0)
                    sb.Append($"\r\n");
                sb.Append(dumpMfu[i]);
            }
            string formatted = sb.ToString();

            tx_identify.Text += formatted;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://shop.kasper.it");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 box = new AboutBox1();
            box.ShowDialog();
        }

        private void bt_show_logmode_Click(object sender, EventArgs e)
        {
            var showLogmode = SendCommand($"LOGMODE?").ToString();
            tx_log.Text += $"{Environment.NewLine}Actual Logmode: {Environment.NewLine}";
            tx_log.Text += showLogmode + $"{Environment.NewLine}";
        }

        private void bt_clearLog_Click(object sender, EventArgs e)
        {
            tx_log.Clear();
        }

        private void cb_log_mode_SelectedIndexChanged(object sender, EventArgs e)
        {
            var changeLogmode = SendCommand($"LOGMODE=" + (cb_log_mode.SelectedItem)).ToString();
            tx_log.Text += $"{Environment.NewLine}Logmode changed to: {cb_log_mode.SelectedItem}{Environment.NewLine}" + changeLogmode;
        }

        private void tx_log_TextChanged(object sender, EventArgs e)
        {
            tx_log.SelectionStart = tx_log.Text.Length;
            tx_log.ScrollToCaret();
        }

        private void tx_output_TextChanged(object sender, EventArgs e)
        {
            tx_output.SelectionStart = tx_output.Text.Length;
            tx_output.ScrollToCaret();
        }

        private void tx_identify_TextChanged(object sender, EventArgs e)
        {
            tx_identify.SelectionStart = tx_identify.Text.Length;
            tx_identify.ScrollToCaret();
        }

        private void bt_logmem_Click(object sender, EventArgs e)
        {
            var showLogmem = SendCommand($"LOGMEM?").ToString();
            tx_log.Text += $"{Environment.NewLine}Logmem: {Environment.NewLine}";
            tx_log.Text += showLogmem + $"{Environment.NewLine}";
        }

        private void bt_logclear_Click(object sender, EventArgs e)
        {
            SendCommandWithoutResult($"LOGCLEAR");
            tx_log.Text += $"{Environment.NewLine}Log memory is now clear! {Environment.NewLine}";
        }

        // TODO: Find possibilities to show LiveLog 
        private void bt_sniff_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"This functionality will be added soon{Environment.NewLine} Actual change to settting=8 and activate ISO14443A_SNIFF", "Coming Feature", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //Change to slot 8
            SendCommandWithoutResult($"SETTING=8");
            tx_log.Text += $"{Environment.NewLine}Changed to slot 8 !{ Environment.NewLine}";

            //CONFIG as SNIFFER
            SendCommandWithoutResult($"CONFIG=ISO14443A_SNIFF");
            tx_log.Text += $"Configurated: ISO14443A_SNIFF{Environment.NewLine}";

            var activeLogMode = SendCommand($"LOGMODE?").ToString();

            if (activeLogMode == "OFF")
            {
                tx_log.Text += "Logmode need set to Memory or Live";
            }
            else
            {

            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Start("https://shop.kasper.it");
        }

        private void bt_keycalc_Click(object sender, EventArgs e)
        {
            // add mfkey32 support like funcionality from RevE-Rebooted
            //Firmware integration done under gtpy-fork
            #region RevE rebooted adjustment

            //SETTING=cb_slots.SelectedIndex
            SendCommandWithoutResult($"SETTING=" + (cb_slots.SelectedItem));

            this.Cursor = Cursors.WaitCursor;

            //RevE rebooted adjustment for RevG
            {

                 //ToDo: Check Slot number - Select wrong slot in tx_output

                var data = SendCommand($"DETECTION?") as byte[];

                var result = MfKeyAttacks.Attack(data);
                if (string.IsNullOrWhiteSpace(result))
                    result = $"mfkey32 attack failed, no keys found{Environment.NewLine}";

                tx_output.Text += $"[Slot {cb_slots.SelectedItem}]{Environment.NewLine}" +result;
                //tx_output.Text += result;
                
            }
            this.Cursor = Cursors.Default;

            #endregion
        }

        private void bt_Clear_Click(object sender, EventArgs e)
        {
            tx_serialCmd.Text = String.Empty;
            tx_SerialOutput.Text = String.Empty;
        }

        private void tb_serialCmd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char)Keys.Return) return;

            var tb = (TextBox)sender;
            var cmd = tb.Text.Trim();
            if (string.IsNullOrWhiteSpace(cmd)) return;

            Send(cmd);
            tx_serialCmd.Text = String.Empty;
        }

        private void bt_SerialCmd_Click(object sender, EventArgs e)
        {
            var cmd = tx_serialCmd.Text.Trim();
            if (string.IsNullOrWhiteSpace(cmd)) return;

            Send(cmd);
            tx_serialCmd.Text = String.Empty;
        }

        private async void Send(string cmd)   
        {
            const string prompt = "--> ";
            
            //var prompt = "--> ";
            tx_SerialOutput.Text += $"{Environment.NewLine}{prompt}{cmd}";

            //determine if command has return data? 
            var res = await SendCommand_GTPY(cmd);
            tx_SerialOutput.Text += $"{Environment.NewLine}{res}";
        }

        private void tx_SerialOutput_TextChanged(object sender, EventArgs e)
        {
            tx_SerialOutput.SelectionStart = tx_SerialOutput.Text.Length;
            tx_SerialOutput.ScrollToCaret();
        }
    }
}
 