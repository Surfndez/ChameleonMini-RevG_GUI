﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ChameleonMini RevG GUI")]
[assembly: AssemblyDescription("GUI for Chameleon Mini Rev.G - is developed in freetime and OpenSource under GNU General Public License v3.0  ---------------------------------------------------------------------------------- you'll get support to this OpenSource Project at https://gitlab.com/Gtpy/ChameleonMini-RevG_GUI/issues  ---------------------------------------------------------------------------------- If you like to donate my work PayPal.Me/gtpy. I'am more interersted in donations of some chameleon-mini (chinese one, original) - always need some stuff to work with - or buy me a beer. Thanks to Flaticon for the usage of nice icons dedigned by Gregor Cresnar.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("csf2@protonmail.ch")]
[assembly: AssemblyProduct("ChameleonMini RevG GUI")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("EDB6CC4E-A50C-4137-B9B3-54EAF25CD670")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.4")]
[assembly: AssemblyFileVersion("1.0.0.0")]
