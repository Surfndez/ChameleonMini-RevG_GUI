using System;

namespace ChameleonMiniGUI
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.tx_output = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.gb_output = new System.Windows.Forms.GroupBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_lBtnL = new System.Windows.Forms.Label();
            this.lb_rBtnL = new System.Windows.Forms.Label();
            this.lb_lBtn = new System.Windows.Forms.Label();
            this.lb_rBtn = new System.Windows.Forms.Label();
            this.lb_ledRed = new System.Windows.Forms.Label();
            this.lb_ledGreen = new System.Windows.Forms.Label();
            this.cb_led_red = new System.Windows.Forms.ComboBox();
            this.cb_led_green = new System.Windows.Forms.ComboBox();
            this.cb_l_btn_long = new System.Windows.Forms.ComboBox();
            this.cb_r_btn_long = new System.Windows.Forms.ComboBox();
            this.cb_l_btn = new System.Windows.Forms.ComboBox();
            this.cb_r_btn = new System.Windows.Forms.ComboBox();
            this.bt_write_setting = new System.Windows.Forms.Button();
            this.bt_read_setting = new System.Windows.Forms.Button();
            this.gb_bootloader = new System.Windows.Forms.GroupBox();
            this.lb_reset = new System.Windows.Forms.Label();
            this.bt_reset = new System.Windows.Forms.Button();
            this.lb_bootloader = new System.Windows.Forms.Label();
            this.bt_bootloader = new System.Windows.Forms.Button();
            this.gb_connectionSettings = new System.Windows.Forms.GroupBox();
            this.tx_firmware = new System.Windows.Forms.TextBox();
            this.tx_constatus = new System.Windows.Forms.TextBox();
            this.bt_disconnect = new System.Windows.Forms.Button();
            this.bt_connect = new System.Windows.Forms.Button();
            this.gb_rssi = new System.Windows.Forms.GroupBox();
            this.bt_rssirefresh = new System.Windows.Forms.Button();
            this.tx_rssi = new System.Windows.Forms.TextBox();
            this.lb_rssi = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gb_tools = new System.Windows.Forms.GroupBox();
            this.bt_keycalc = new System.Windows.Forms.Button();
            this.bt_download = new System.Windows.Forms.Button();
            this.bt_upload = new System.Windows.Forms.Button();
            this.lb_toolText = new System.Windows.Forms.Label();
            this.gb_newConf = new System.Windows.Forms.GroupBox();
            this.lb_setConfig = new System.Windows.Forms.Label();
            this.cb_config_new = new System.Windows.Forms.ComboBox();
            this.gb_SlotControl = new System.Windows.Forms.GroupBox();
            this.bt_refresh = new System.Windows.Forms.Button();
            this.bt_clear_slot = new System.Windows.Forms.Button();
            this.cb_slots = new System.Windows.Forms.ComboBox();
            this.lb_slot = new System.Windows.Forms.Label();
            this.gb_SlotData = new System.Windows.Forms.GroupBox();
            this.bt_clone = new System.Windows.Forms.Button();
            this.bt_change_uid = new System.Windows.Forms.Button();
            this.tx_conf_show = new System.Windows.Forms.TextBox();
            this.tx_size = new System.Windows.Forms.TextBox();
            this.lb_size = new System.Windows.Forms.Label();
            this.tx_uid = new System.Windows.Forms.TextBox();
            this.lb_uid = new System.Windows.Forms.Label();
            this.lb_config = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lb_warn = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tx_identify = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.bt_clear_identify_log = new System.Windows.Forms.Button();
            this.bt_read_uid = new System.Windows.Forms.Button();
            this.bt_ultralight_read = new System.Windows.Forms.Button();
            this.bt_identify = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lb_warn2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tx_log = new System.Windows.Forms.TextBox();
            this.bt_clearLog = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bt_sniff = new System.Windows.Forms.Button();
            this.bt_logclear = new System.Windows.Forms.Button();
            this.bt_logmem = new System.Windows.Forms.Button();
            this.bt_show_logmode = new System.Windows.Forms.Button();
            this.lb_logmode = new System.Windows.Forms.Label();
            this.cb_log_mode = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gb_serial = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tx_SerialHelp = new System.Windows.Forms.TextBox();
            this.bt_Clear = new System.Windows.Forms.Button();
            this.bt_SerialCmd = new System.Windows.Forms.Button();
            this.tx_SerialOutput = new System.Windows.Forms.TextBox();
            this.tx_serialCmd = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.gb_charging = new System.Windows.Forms.GroupBox();
            this.tx_chrg = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tx_thres = new System.Windows.Forms.TextBox();
            this.toolTip_refresh = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip_reset = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip_clone = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip_rssi = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip_chnguid = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.chameleonMiniGUIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip_link_kaos = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.toolTip_sniff = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip_upload = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip_download = new System.Windows.Forms.ToolTip(this.components);
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.gb_output.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gb_bootloader.SuspendLayout();
            this.gb_connectionSettings.SuspendLayout();
            this.gb_rssi.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gb_tools.SuspendLayout();
            this.gb_newConf.SuspendLayout();
            this.gb_SlotControl.SuspendLayout();
            this.gb_SlotData.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.gb_serial.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.gb_charging.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tx_output
            // 
            this.tx_output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tx_output.Location = new System.Drawing.Point(3, 16);
            this.tx_output.Multiline = true;
            this.tx_output.Name = "tx_output";
            this.tx_output.ReadOnly = true;
            this.tx_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tx_output.Size = new System.Drawing.Size(220, 143);
            this.tx_output.TabIndex = 6;
            this.tx_output.TextChanged += new System.EventHandler(this.tx_output_TextChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Dump files| *.bin; *.dump; *.mdf; *.hex";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Dump files| *.bin; *.dump; *.mdf; *.hex";
            // 
            // gb_output
            // 
            this.gb_output.Controls.Add(this.tx_output);
            this.gb_output.Location = new System.Drawing.Point(843, 28);
            this.gb_output.Name = "gb_output";
            this.gb_output.Size = new System.Drawing.Size(226, 162);
            this.gb_output.TabIndex = 7;
            this.gb_output.TabStop = false;
            this.gb_output.Text = "Output";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.gb_bootloader);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(824, 333);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::ChameleonMiniGUI.Properties.Resources.chamRevG_detail_mini_scaled;
            this.pictureBox1.Location = new System.Drawing.Point(44, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(258, 191);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lb_lBtnL);
            this.groupBox1.Controls.Add(this.lb_rBtnL);
            this.groupBox1.Controls.Add(this.lb_lBtn);
            this.groupBox1.Controls.Add(this.lb_rBtn);
            this.groupBox1.Controls.Add(this.lb_ledRed);
            this.groupBox1.Controls.Add(this.lb_ledGreen);
            this.groupBox1.Controls.Add(this.cb_led_red);
            this.groupBox1.Controls.Add(this.cb_led_green);
            this.groupBox1.Controls.Add(this.cb_l_btn_long);
            this.groupBox1.Controls.Add(this.cb_r_btn_long);
            this.groupBox1.Controls.Add(this.cb_l_btn);
            this.groupBox1.Controls.Add(this.cb_r_btn);
            this.groupBox1.Controls.Add(this.bt_write_setting);
            this.groupBox1.Controls.Add(this.bt_read_setting);
            this.groupBox1.Location = new System.Drawing.Point(308, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(434, 208);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // lb_lBtnL
            // 
            this.lb_lBtnL.AutoSize = true;
            this.lb_lBtnL.Location = new System.Drawing.Point(29, 166);
            this.lb_lBtnL.Name = "lb_lBtnL";
            this.lb_lBtnL.Size = new System.Drawing.Size(73, 13);
            this.lb_lBtnL.TabIndex = 13;
            this.lb_lBtnL.Text = "L. Button long";
            // 
            // lb_rBtnL
            // 
            this.lb_rBtnL.AutoSize = true;
            this.lb_rBtnL.Location = new System.Drawing.Point(29, 139);
            this.lb_rBtnL.Name = "lb_rBtnL";
            this.lb_rBtnL.Size = new System.Drawing.Size(75, 13);
            this.lb_rBtnL.TabIndex = 12;
            this.lb_rBtnL.Text = "R. Button long";
            // 
            // lb_lBtn
            // 
            this.lb_lBtn.AutoSize = true;
            this.lb_lBtn.Location = new System.Drawing.Point(29, 112);
            this.lb_lBtn.Name = "lb_lBtn";
            this.lb_lBtn.Size = new System.Drawing.Size(50, 13);
            this.lb_lBtn.TabIndex = 11;
            this.lb_lBtn.Text = "L. Button";
            // 
            // lb_rBtn
            // 
            this.lb_rBtn.AutoSize = true;
            this.lb_rBtn.Location = new System.Drawing.Point(29, 85);
            this.lb_rBtn.Name = "lb_rBtn";
            this.lb_rBtn.Size = new System.Drawing.Size(52, 13);
            this.lb_rBtn.TabIndex = 10;
            this.lb_rBtn.Text = "R. Button";
            // 
            // lb_ledRed
            // 
            this.lb_ledRed.AutoSize = true;
            this.lb_ledRed.BackColor = System.Drawing.Color.Red;
            this.lb_ledRed.Location = new System.Drawing.Point(29, 58);
            this.lb_ledRed.Name = "lb_ledRed";
            this.lb_ledRed.Size = new System.Drawing.Size(51, 13);
            this.lb_ledRed.TabIndex = 9;
            this.lb_ledRed.Text = "LED Red";
            // 
            // lb_ledGreen
            // 
            this.lb_ledGreen.AutoSize = true;
            this.lb_ledGreen.BackColor = System.Drawing.Color.LawnGreen;
            this.lb_ledGreen.Location = new System.Drawing.Point(29, 33);
            this.lb_ledGreen.Name = "lb_ledGreen";
            this.lb_ledGreen.Size = new System.Drawing.Size(60, 13);
            this.lb_ledGreen.TabIndex = 8;
            this.lb_ledGreen.Text = "LED Green";
            // 
            // cb_led_red
            // 
            this.cb_led_red.FormattingEnabled = true;
            this.cb_led_red.Items.AddRange(new object[] {
            "NONE",
            "POWERED",
            "TERMINAL_CONN",
            "TERMINAL_RXTX",
            "SETTING_CHANGE",
            "MEMORY_STORED",
            "MEMORY_CHANGED",
            "CODEC_RX",
            "CODEC_TX",
            "FIELD_DETECTED",
            "LOGMEM_FULL"});
            this.cb_led_red.Location = new System.Drawing.Point(108, 55);
            this.cb_led_red.Name = "cb_led_red";
            this.cb_led_red.Size = new System.Drawing.Size(179, 21);
            this.cb_led_red.TabIndex = 7;
            // 
            // cb_led_green
            // 
            this.cb_led_green.FormattingEnabled = true;
            this.cb_led_green.Items.AddRange(new object[] {
            "NONE",
            "POWERED",
            "TERMINAL_CONN",
            "TERMINAL_RXTX",
            "SETTING_CHANGE",
            "MEMORY_STORED",
            "MEMORY_CHANGED",
            "CODEC_RX",
            "CODEC_TX",
            "FIELD_DETECTED",
            "LOGMEM_FULL"});
            this.cb_led_green.Location = new System.Drawing.Point(108, 30);
            this.cb_led_green.Name = "cb_led_green";
            this.cb_led_green.Size = new System.Drawing.Size(179, 21);
            this.cb_led_green.TabIndex = 6;
            // 
            // cb_l_btn_long
            // 
            this.cb_l_btn_long.FormattingEnabled = true;
            this.cb_l_btn_long.Items.AddRange(new object[] {
            "NONE",
            "UID_RANDOM",
            "UID_LEFT_INCREMENT",
            "UID_RIGHT_INCREMENT",
            "UID_LEFT_DECREMENT",
            "UID_RIGHT_DECREMENT",
            "CYCLE_SETTINGS",
            "STORE_MEM",
            "RECALL_MEM",
            "TOGGLE_FIELD",
            "STORE_LOG",
            "CLONE"});
            this.cb_l_btn_long.Location = new System.Drawing.Point(108, 163);
            this.cb_l_btn_long.Name = "cb_l_btn_long";
            this.cb_l_btn_long.Size = new System.Drawing.Size(179, 21);
            this.cb_l_btn_long.TabIndex = 5;
            // 
            // cb_r_btn_long
            // 
            this.cb_r_btn_long.FormattingEnabled = true;
            this.cb_r_btn_long.Items.AddRange(new object[] {
            "NONE",
            "UID_RANDOM",
            "UID_LEFT_INCREMENT",
            "UID_RIGHT_INCREMENT",
            "UID_LEFT_DECREMENT",
            "UID_RIGHT_DECREMENT",
            "CYCLE_SETTINGS",
            "STORE_MEM",
            "RECALL_MEM",
            "TOGGLE_FIELD",
            "STORE_LOG",
            "CLONE"});
            this.cb_r_btn_long.Location = new System.Drawing.Point(108, 136);
            this.cb_r_btn_long.Name = "cb_r_btn_long";
            this.cb_r_btn_long.Size = new System.Drawing.Size(179, 21);
            this.cb_r_btn_long.TabIndex = 4;
            // 
            // cb_l_btn
            // 
            this.cb_l_btn.FormattingEnabled = true;
            this.cb_l_btn.Items.AddRange(new object[] {
            "NONE",
            "UID_RANDOM",
            "UID_LEFT_INCREMENT",
            "UID_RIGHT_INCREMENT",
            "UID_LEFT_DECREMENT",
            "UID_RIGHT_DECREMENT",
            "CYCLE_SETTINGS",
            "STORE_MEM",
            "RECALL_MEM",
            "TOGGLE_FIELD",
            "STORE_LOG",
            "CLONE"});
            this.cb_l_btn.Location = new System.Drawing.Point(108, 109);
            this.cb_l_btn.Name = "cb_l_btn";
            this.cb_l_btn.Size = new System.Drawing.Size(179, 21);
            this.cb_l_btn.TabIndex = 3;
            // 
            // cb_r_btn
            // 
            this.cb_r_btn.FormattingEnabled = true;
            this.cb_r_btn.Items.AddRange(new object[] {
            "NONE",
            "UID_RANDOM",
            "UID_LEFT_INCREMENT",
            "UID_RIGHT_INCREMENT",
            "UID_LEFT_DECREMENT",
            "UID_RIGHT_DECREMENT",
            "CYCLE_SETTINGS",
            "STORE_MEM",
            "RECALL_MEM",
            "TOGGLE_FIELD",
            "STORE_LOG",
            "CLONE"});
            this.cb_r_btn.Location = new System.Drawing.Point(108, 82);
            this.cb_r_btn.Name = "cb_r_btn";
            this.cb_r_btn.Size = new System.Drawing.Size(179, 21);
            this.cb_r_btn.TabIndex = 2;
            // 
            // bt_write_setting
            // 
            this.bt_write_setting.Location = new System.Drawing.Point(327, 55);
            this.bt_write_setting.Name = "bt_write_setting";
            this.bt_write_setting.Size = new System.Drawing.Size(91, 23);
            this.bt_write_setting.TabIndex = 1;
            this.bt_write_setting.Text = "Write Settings";
            this.bt_write_setting.UseVisualStyleBackColor = true;
            this.bt_write_setting.Click += new System.EventHandler(this.bt_write_setting_Click);
            // 
            // bt_read_setting
            // 
            this.bt_read_setting.Location = new System.Drawing.Point(327, 28);
            this.bt_read_setting.Name = "bt_read_setting";
            this.bt_read_setting.Size = new System.Drawing.Size(91, 23);
            this.bt_read_setting.TabIndex = 0;
            this.bt_read_setting.Text = "Read Settings";
            this.bt_read_setting.UseVisualStyleBackColor = true;
            this.bt_read_setting.Click += new System.EventHandler(this.bt_read_setting_Click);
            // 
            // gb_bootloader
            // 
            this.gb_bootloader.Controls.Add(this.lb_reset);
            this.gb_bootloader.Controls.Add(this.bt_reset);
            this.gb_bootloader.Controls.Add(this.lb_bootloader);
            this.gb_bootloader.Controls.Add(this.bt_bootloader);
            this.gb_bootloader.Location = new System.Drawing.Point(44, 233);
            this.gb_bootloader.Name = "gb_bootloader";
            this.gb_bootloader.Size = new System.Drawing.Size(698, 88);
            this.gb_bootloader.TabIndex = 2;
            this.gb_bootloader.TabStop = false;
            this.gb_bootloader.Text = "Misc.";
            // 
            // lb_reset
            // 
            this.lb_reset.AutoSize = true;
            this.lb_reset.Location = new System.Drawing.Point(111, 24);
            this.lb_reset.Name = "lb_reset";
            this.lb_reset.Size = new System.Drawing.Size(121, 13);
            this.lb_reset.TabIndex = 9;
            this.lb_reset.Text = "Reboots the Chameleon";
            // 
            // bt_reset
            // 
            this.bt_reset.Location = new System.Drawing.Point(15, 19);
            this.bt_reset.Name = "bt_reset";
            this.bt_reset.Size = new System.Drawing.Size(82, 23);
            this.bt_reset.TabIndex = 8;
            this.bt_reset.Text = "Reset";
            this.toolTip_reset.SetToolTip(this.bt_reset, "After RESET need to connect again");
            this.bt_reset.UseVisualStyleBackColor = true;
            this.bt_reset.Click += new System.EventHandler(this.bt_reset_Click);
            // 
            // lb_bootloader
            // 
            this.lb_bootloader.AutoSize = true;
            this.lb_bootloader.Location = new System.Drawing.Point(111, 53);
            this.lb_bootloader.Name = "lb_bootloader";
            this.lb_bootloader.Size = new System.Drawing.Size(267, 13);
            this.lb_bootloader.TabIndex = 7;
            this.lb_bootloader.Text = "Turn the Chameleon into firmware upgrade mode (DFU)";
            // 
            // bt_bootloader
            // 
            this.bt_bootloader.Location = new System.Drawing.Point(15, 48);
            this.bt_bootloader.Name = "bt_bootloader";
            this.bt_bootloader.Size = new System.Drawing.Size(82, 23);
            this.bt_bootloader.TabIndex = 6;
            this.bt_bootloader.Text = "Upgrade";
            this.bt_bootloader.UseVisualStyleBackColor = true;
            this.bt_bootloader.Click += new System.EventHandler(this.bt_bootloader_Click);
            // 
            // gb_connectionSettings
            // 
            this.gb_connectionSettings.Controls.Add(this.tx_firmware);
            this.gb_connectionSettings.Controls.Add(this.tx_constatus);
            this.gb_connectionSettings.Controls.Add(this.bt_disconnect);
            this.gb_connectionSettings.Controls.Add(this.bt_connect);
            this.gb_connectionSettings.Location = new System.Drawing.Point(12, 24);
            this.gb_connectionSettings.Name = "gb_connectionSettings";
            this.gb_connectionSettings.Size = new System.Drawing.Size(510, 162);
            this.gb_connectionSettings.TabIndex = 3;
            this.gb_connectionSettings.TabStop = false;
            this.gb_connectionSettings.Text = "Connection status";
            // 
            // tx_firmware
            // 
            this.tx_firmware.Enabled = false;
            this.tx_firmware.Location = new System.Drawing.Point(44, 54);
            this.tx_firmware.Multiline = true;
            this.tx_firmware.Name = "tx_firmware";
            this.tx_firmware.ReadOnly = true;
            this.tx_firmware.Size = new System.Drawing.Size(379, 47);
            this.tx_firmware.TabIndex = 6;
            // 
            // tx_constatus
            // 
            this.tx_constatus.Location = new System.Drawing.Point(44, 26);
            this.tx_constatus.Name = "tx_constatus";
            this.tx_constatus.ReadOnly = true;
            this.tx_constatus.Size = new System.Drawing.Size(379, 20);
            this.tx_constatus.TabIndex = 5;
            this.tx_constatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bt_disconnect
            // 
            this.bt_disconnect.Enabled = false;
            this.bt_disconnect.Location = new System.Drawing.Point(429, 54);
            this.bt_disconnect.Name = "bt_disconnect";
            this.bt_disconnect.Size = new System.Drawing.Size(75, 20);
            this.bt_disconnect.TabIndex = 1;
            this.bt_disconnect.Text = "Disconnect";
            this.bt_disconnect.UseVisualStyleBackColor = true;
            this.bt_disconnect.Click += new System.EventHandler(this.bt_disconnect_Click);
            // 
            // bt_connect
            // 
            this.bt_connect.Location = new System.Drawing.Point(429, 26);
            this.bt_connect.Name = "bt_connect";
            this.bt_connect.Size = new System.Drawing.Size(75, 20);
            this.bt_connect.TabIndex = 0;
            this.bt_connect.Text = "Connect";
            this.bt_connect.UseVisualStyleBackColor = true;
            this.bt_connect.Click += new System.EventHandler(this.bt_connect_Click);
            // 
            // gb_rssi
            // 
            this.gb_rssi.Controls.Add(this.bt_rssirefresh);
            this.gb_rssi.Controls.Add(this.tx_rssi);
            this.gb_rssi.Controls.Add(this.lb_rssi);
            this.gb_rssi.Location = new System.Drawing.Point(528, 28);
            this.gb_rssi.Name = "gb_rssi";
            this.gb_rssi.Size = new System.Drawing.Size(226, 74);
            this.gb_rssi.TabIndex = 0;
            this.gb_rssi.TabStop = false;
            this.gb_rssi.Text = "RSSI Voltage";
            // 
            // bt_rssirefresh
            // 
            this.bt_rssirefresh.Enabled = false;
            this.bt_rssirefresh.Location = new System.Drawing.Point(143, 42);
            this.bt_rssirefresh.Name = "bt_rssirefresh";
            this.bt_rssirefresh.Size = new System.Drawing.Size(53, 20);
            this.bt_rssirefresh.TabIndex = 2;
            this.bt_rssirefresh.Text = "Refresh";
            this.toolTip_rssi.SetToolTip(this.bt_rssirefresh, "Returns the voltage measured at the antenna of the Chameleon, \r\ne.g., to detect t" +
        "he presence of an RF field \r\nor compare the field strength of different RFID rea" +
        "ders. ");
            this.bt_rssirefresh.UseVisualStyleBackColor = true;
            this.bt_rssirefresh.Click += new System.EventHandler(this.bt_rssirefresh_Click);
            // 
            // tx_rssi
            // 
            this.tx_rssi.Location = new System.Drawing.Point(37, 42);
            this.tx_rssi.Name = "tx_rssi";
            this.tx_rssi.ReadOnly = true;
            this.tx_rssi.Size = new System.Drawing.Size(100, 20);
            this.tx_rssi.TabIndex = 1;
            // 
            // lb_rssi
            // 
            this.lb_rssi.AutoSize = true;
            this.lb_rssi.Location = new System.Drawing.Point(34, 26);
            this.lb_rssi.Name = "lb_rssi";
            this.lb_rssi.Size = new System.Drawing.Size(69, 13);
            this.lb_rssi.TabIndex = 0;
            this.lb_rssi.Text = "Current RSSI";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gb_tools);
            this.tabPage1.Controls.Add(this.gb_newConf);
            this.tabPage1.Controls.Add(this.gb_SlotControl);
            this.tabPage1.Controls.Add(this.gb_SlotData);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(824, 333);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Slots";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gb_tools
            // 
            this.gb_tools.Controls.Add(this.bt_keycalc);
            this.gb_tools.Controls.Add(this.bt_download);
            this.gb_tools.Controls.Add(this.bt_upload);
            this.gb_tools.Controls.Add(this.lb_toolText);
            this.gb_tools.Location = new System.Drawing.Point(44, 249);
            this.gb_tools.Name = "gb_tools";
            this.gb_tools.Size = new System.Drawing.Size(698, 69);
            this.gb_tools.TabIndex = 39;
            this.gb_tools.TabStop = false;
            this.gb_tools.Text = "Tools";
            // 
            // bt_keycalc
            // 
            this.bt_keycalc.Location = new System.Drawing.Point(525, 11);
            this.bt_keycalc.Name = "bt_keycalc";
            this.bt_keycalc.Size = new System.Drawing.Size(75, 23);
            this.bt_keycalc.TabIndex = 19;
            this.bt_keycalc.Text = "mfkey32";
            this.bt_keycalc.UseVisualStyleBackColor = true;
            this.bt_keycalc.Click += new System.EventHandler(this.bt_keycalc_Click);
            // 
            // bt_download
            // 
            this.bt_download.Location = new System.Drawing.Point(606, 40);
            this.bt_download.Name = "bt_download";
            this.bt_download.Size = new System.Drawing.Size(76, 23);
            this.bt_download.TabIndex = 18;
            this.bt_download.Text = "Download";
            this.toolTip_download.SetToolTip(this.bt_download, "Download a virtualized card with the current memory size");
            this.bt_download.UseVisualStyleBackColor = true;
            this.bt_download.Click += new System.EventHandler(this.bt_download_Click);
            // 
            // bt_upload
            // 
            this.bt_upload.Location = new System.Drawing.Point(606, 11);
            this.bt_upload.Name = "bt_upload";
            this.bt_upload.Size = new System.Drawing.Size(76, 23);
            this.bt_upload.TabIndex = 17;
            this.bt_upload.Text = "Upload";
            this.toolTip_upload.SetToolTip(this.bt_upload, "Upload a new virtualized card into the currently selected slot, with a size up to" +
        " the current memory size ");
            this.bt_upload.UseVisualStyleBackColor = true;
            this.bt_upload.Click += new System.EventHandler(this.bt_upload_Click);
            // 
            // lb_toolText
            // 
            this.lb_toolText.AutoSize = true;
            this.lb_toolText.ForeColor = System.Drawing.Color.Blue;
            this.lb_toolText.Location = new System.Drawing.Point(6, 32);
            this.lb_toolText.Name = "lb_toolText";
            this.lb_toolText.Size = new System.Drawing.Size(181, 13);
            this.lb_toolText.TabIndex = 16;
            this.lb_toolText.Text = "More functionality will be added soon";
            // 
            // gb_newConf
            // 
            this.gb_newConf.Controls.Add(this.lb_setConfig);
            this.gb_newConf.Controls.Add(this.cb_config_new);
            this.gb_newConf.Location = new System.Drawing.Point(429, 95);
            this.gb_newConf.Name = "gb_newConf";
            this.gb_newConf.Size = new System.Drawing.Size(313, 148);
            this.gb_newConf.TabIndex = 38;
            this.gb_newConf.TabStop = false;
            this.gb_newConf.Text = "New Configuration";
            // 
            // lb_setConfig
            // 
            this.lb_setConfig.AutoSize = true;
            this.lb_setConfig.Location = new System.Drawing.Point(6, 39);
            this.lb_setConfig.Name = "lb_setConfig";
            this.lb_setConfig.Size = new System.Drawing.Size(111, 13);
            this.lb_setConfig.TabIndex = 1;
            this.lb_setConfig.Text = "Set new Configuration";
            // 
            // cb_config_new
            // 
            this.cb_config_new.FormattingEnabled = true;
            this.cb_config_new.Items.AddRange(new object[] {
            "NONE",
            "MF_ULTRALIGHT",
            "MF_ULTRALIGHT_EV1_80B",
            "MF_ULTRALIGHT_EV1_164B",
            "MF_CLASSIC_1K",
            "MF_CLASSIC_1K_7B",
            "MF_CLASSIC_4K",
            "MF_CLASSIC_4K_7B",
            "ISO14443A_SNIFF",
            "ISO14443A_READER",
            "MF_DETECTION"});
            this.cb_config_new.Location = new System.Drawing.Point(124, 35);
            this.cb_config_new.Name = "cb_config_new";
            this.cb_config_new.Size = new System.Drawing.Size(155, 21);
            this.cb_config_new.TabIndex = 0;
            this.cb_config_new.SelectedIndexChanged += new System.EventHandler(this.cb_config_new_SelectedIndexChanged);
            // 
            // gb_SlotControl
            // 
            this.gb_SlotControl.Controls.Add(this.bt_refresh);
            this.gb_SlotControl.Controls.Add(this.bt_clear_slot);
            this.gb_SlotControl.Controls.Add(this.cb_slots);
            this.gb_SlotControl.Controls.Add(this.lb_slot);
            this.gb_SlotControl.Location = new System.Drawing.Point(44, 19);
            this.gb_SlotControl.Name = "gb_SlotControl";
            this.gb_SlotControl.Size = new System.Drawing.Size(698, 70);
            this.gb_SlotControl.TabIndex = 37;
            this.gb_SlotControl.TabStop = false;
            this.gb_SlotControl.Text = "Slot Control";
            // 
            // bt_refresh
            // 
            this.bt_refresh.Location = new System.Drawing.Point(247, 31);
            this.bt_refresh.Name = "bt_refresh";
            this.bt_refresh.Size = new System.Drawing.Size(75, 23);
            this.bt_refresh.TabIndex = 20;
            this.bt_refresh.Text = "Refresh";
            this.toolTip_refresh.SetToolTip(this.bt_refresh, "Refresh data if changed by device buttons");
            this.bt_refresh.UseVisualStyleBackColor = true;
            this.bt_refresh.Click += new System.EventHandler(this.bt_refresh_Click);
            // 
            // bt_clear_slot
            // 
            this.bt_clear_slot.Location = new System.Drawing.Point(166, 31);
            this.bt_clear_slot.Name = "bt_clear_slot";
            this.bt_clear_slot.Size = new System.Drawing.Size(75, 23);
            this.bt_clear_slot.TabIndex = 19;
            this.bt_clear_slot.Text = "Clear Slot";
            this.bt_clear_slot.UseVisualStyleBackColor = true;
            this.bt_clear_slot.Click += new System.EventHandler(this.bt_clear_slot_Click);
            // 
            // cb_slots
            // 
            this.cb_slots.FormattingEnabled = true;
            this.cb_slots.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cb_slots.Location = new System.Drawing.Point(112, 33);
            this.cb_slots.Name = "cb_slots";
            this.cb_slots.Size = new System.Drawing.Size(48, 21);
            this.cb_slots.TabIndex = 17;
            this.cb_slots.SelectedIndexChanged += new System.EventHandler(this.cb_slots_SelectedIndexChanged);
            // 
            // lb_slot
            // 
            this.lb_slot.AutoSize = true;
            this.lb_slot.Location = new System.Drawing.Point(6, 36);
            this.lb_slot.Name = "lb_slot";
            this.lb_slot.Size = new System.Drawing.Size(100, 13);
            this.lb_slot.TabIndex = 16;
            this.lb_slot.Text = "Change active Slot ";
            // 
            // gb_SlotData
            // 
            this.gb_SlotData.Controls.Add(this.bt_clone);
            this.gb_SlotData.Controls.Add(this.bt_change_uid);
            this.gb_SlotData.Controls.Add(this.tx_conf_show);
            this.gb_SlotData.Controls.Add(this.tx_size);
            this.gb_SlotData.Controls.Add(this.lb_size);
            this.gb_SlotData.Controls.Add(this.tx_uid);
            this.gb_SlotData.Controls.Add(this.lb_uid);
            this.gb_SlotData.Controls.Add(this.lb_config);
            this.gb_SlotData.Location = new System.Drawing.Point(44, 95);
            this.gb_SlotData.Name = "gb_SlotData";
            this.gb_SlotData.Size = new System.Drawing.Size(379, 148);
            this.gb_SlotData.TabIndex = 21;
            this.gb_SlotData.TabStop = false;
            this.gb_SlotData.Text = "Data in actual Slot";
            // 
            // bt_clone
            // 
            this.bt_clone.BackColor = System.Drawing.Color.Transparent;
            this.bt_clone.Location = new System.Drawing.Point(247, 91);
            this.bt_clone.Name = "bt_clone";
            this.bt_clone.Size = new System.Drawing.Size(86, 23);
            this.bt_clone.TabIndex = 22;
            this.bt_clone.Text = "CLONE";
            this.toolTip_clone.SetToolTip(this.bt_clone, "Send the CLONE command and switch active slot to reader, read config \r\n& uid from" +
        " card and reconfig active slot to card & uid (simple uid clone)");
            this.bt_clone.UseVisualStyleBackColor = false;
            this.bt_clone.Click += new System.EventHandler(this.bt_clone_Click);
            // 
            // bt_change_uid
            // 
            this.bt_change_uid.Location = new System.Drawing.Point(247, 62);
            this.bt_change_uid.Name = "bt_change_uid";
            this.bt_change_uid.Size = new System.Drawing.Size(86, 23);
            this.bt_change_uid.TabIndex = 21;
            this.bt_change_uid.Text = "Change UID";
            this.toolTip_chnguid.SetToolTip(this.bt_change_uid, "Change the UID of active slot to the UID\r\nmaintained in the UID field");
            this.bt_change_uid.UseVisualStyleBackColor = true;
            this.bt_change_uid.Click += new System.EventHandler(this.bt_change_uid_Click);
            // 
            // tx_conf_show
            // 
            this.tx_conf_show.Location = new System.Drawing.Point(81, 36);
            this.tx_conf_show.Name = "tx_conf_show";
            this.tx_conf_show.ReadOnly = true;
            this.tx_conf_show.Size = new System.Drawing.Size(160, 20);
            this.tx_conf_show.TabIndex = 20;
            // 
            // tx_size
            // 
            this.tx_size.Location = new System.Drawing.Point(81, 93);
            this.tx_size.Name = "tx_size";
            this.tx_size.ReadOnly = true;
            this.tx_size.Size = new System.Drawing.Size(160, 20);
            this.tx_size.TabIndex = 15;
            // 
            // lb_size
            // 
            this.lb_size.AutoSize = true;
            this.lb_size.Location = new System.Drawing.Point(6, 96);
            this.lb_size.Name = "lb_size";
            this.lb_size.Size = new System.Drawing.Size(49, 13);
            this.lb_size.TabIndex = 14;
            this.lb_size.Text = "UID Size";
            // 
            // tx_uid
            // 
            this.tx_uid.Location = new System.Drawing.Point(81, 64);
            this.tx_uid.Name = "tx_uid";
            this.tx_uid.Size = new System.Drawing.Size(160, 20);
            this.tx_uid.TabIndex = 3;
            // 
            // lb_uid
            // 
            this.lb_uid.AutoSize = true;
            this.lb_uid.Location = new System.Drawing.Point(6, 67);
            this.lb_uid.Name = "lb_uid";
            this.lb_uid.Size = new System.Drawing.Size(26, 13);
            this.lb_uid.TabIndex = 2;
            this.lb_uid.Text = "UID";
            // 
            // lb_config
            // 
            this.lb_config.AutoSize = true;
            this.lb_config.Location = new System.Drawing.Point(6, 36);
            this.lb_config.Name = "lb_config";
            this.lb_config.Size = new System.Drawing.Size(69, 13);
            this.lb_config.TabIndex = 1;
            this.lb_config.Text = "Configuration";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Enabled = false;
            this.tabControl1.Location = new System.Drawing.Point(12, 192);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(832, 359);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lb_warn);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(824, 333);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Identify";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lb_warn
            // 
            this.lb_warn.AutoSize = true;
            this.lb_warn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_warn.ForeColor = System.Drawing.Color.Red;
            this.lb_warn.Location = new System.Drawing.Point(295, 10);
            this.lb_warn.Name = "lb_warn";
            this.lb_warn.Size = new System.Drawing.Size(304, 13);
            this.lb_warn.TabIndex = 5;
            this.lb_warn.Text = "WARNING: Using this feature might compromise data in slot 8 !";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tx_identify);
            this.groupBox7.Location = new System.Drawing.Point(295, 38);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(517, 284);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Log";
            // 
            // tx_identify
            // 
            this.tx_identify.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tx_identify.Location = new System.Drawing.Point(3, 16);
            this.tx_identify.Multiline = true;
            this.tx_identify.Name = "tx_identify";
            this.tx_identify.ReadOnly = true;
            this.tx_identify.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tx_identify.Size = new System.Drawing.Size(511, 265);
            this.tx_identify.TabIndex = 0;
            this.tx_identify.Text = "waiting for command...\r\nPlease put your RFID Card on the antenna from your Chamel" +
    "eon...\r\n ";
            this.tx_identify.TextChanged += new System.EventHandler(this.tx_identify_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.bt_clear_identify_log);
            this.groupBox6.Controls.Add(this.bt_read_uid);
            this.groupBox6.Controls.Add(this.bt_ultralight_read);
            this.groupBox6.Controls.Add(this.bt_identify);
            this.groupBox6.Location = new System.Drawing.Point(40, 38);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(245, 284);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Identify Controls";
            // 
            // bt_clear_identify_log
            // 
            this.bt_clear_identify_log.Location = new System.Drawing.Point(164, 19);
            this.bt_clear_identify_log.Name = "bt_clear_identify_log";
            this.bt_clear_identify_log.Size = new System.Drawing.Size(75, 23);
            this.bt_clear_identify_log.TabIndex = 3;
            this.bt_clear_identify_log.Text = "Clear Log";
            this.bt_clear_identify_log.UseVisualStyleBackColor = true;
            this.bt_clear_identify_log.Click += new System.EventHandler(this.bt_clear_identify_log_Click);
            // 
            // bt_read_uid
            // 
            this.bt_read_uid.Location = new System.Drawing.Point(6, 19);
            this.bt_read_uid.Name = "bt_read_uid";
            this.bt_read_uid.Size = new System.Drawing.Size(86, 23);
            this.bt_read_uid.TabIndex = 0;
            this.bt_read_uid.Text = "Read UID";
            this.bt_read_uid.UseVisualStyleBackColor = true;
            this.bt_read_uid.Click += new System.EventHandler(this.bt_read_uid_Click);
            // 
            // bt_ultralight_read
            // 
            this.bt_ultralight_read.Image = ((System.Drawing.Image)(resources.GetObject("bt_ultralight_read.Image")));
            this.bt_ultralight_read.Location = new System.Drawing.Point(6, 77);
            this.bt_ultralight_read.Name = "bt_ultralight_read";
            this.bt_ultralight_read.Size = new System.Drawing.Size(86, 47);
            this.bt_ultralight_read.TabIndex = 2;
            this.bt_ultralight_read.Text = "Read MF Ultralight Content";
            this.bt_ultralight_read.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.bt_ultralight_read.UseVisualStyleBackColor = true;
            this.bt_ultralight_read.Click += new System.EventHandler(this.bt_ultralight_read_Click);
            // 
            // bt_identify
            // 
            this.bt_identify.Location = new System.Drawing.Point(6, 48);
            this.bt_identify.Name = "bt_identify";
            this.bt_identify.Size = new System.Drawing.Size(86, 23);
            this.bt_identify.TabIndex = 1;
            this.bt_identify.Text = "Identify";
            this.bt_identify.UseVisualStyleBackColor = true;
            this.bt_identify.Click += new System.EventHandler(this.bt_identify_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.lb_warn2);
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.bt_clearLog);
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(824, 333);
            this.tabPage4.TabIndex = 5;
            this.tabPage4.Text = "Logging";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // lb_warn2
            // 
            this.lb_warn2.AutoSize = true;
            this.lb_warn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_warn2.ForeColor = System.Drawing.Color.Red;
            this.lb_warn2.Location = new System.Drawing.Point(329, 6);
            this.lb_warn2.Name = "lb_warn2";
            this.lb_warn2.Size = new System.Drawing.Size(325, 13);
            this.lb_warn2.TabIndex = 22;
            this.lb_warn2.Text = "WARNING: Using the sniff feature might compromise data in slot 8 !";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tx_log);
            this.groupBox4.Location = new System.Drawing.Point(329, 35);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(475, 271);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Log";
            // 
            // tx_log
            // 
            this.tx_log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tx_log.Location = new System.Drawing.Point(3, 16);
            this.tx_log.Multiline = true;
            this.tx_log.Name = "tx_log";
            this.tx_log.ReadOnly = true;
            this.tx_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tx_log.Size = new System.Drawing.Size(469, 252);
            this.tx_log.TabIndex = 0;
            this.tx_log.Text = "... waiting for command ...\r\n";
            this.tx_log.TextChanged += new System.EventHandler(this.tx_log_TextChanged);
            // 
            // bt_clearLog
            // 
            this.bt_clearLog.Location = new System.Drawing.Point(729, 6);
            this.bt_clearLog.Name = "bt_clearLog";
            this.bt_clearLog.Size = new System.Drawing.Size(75, 23);
            this.bt_clearLog.TabIndex = 21;
            this.bt_clearLog.Text = "Clear Log";
            this.bt_clearLog.UseVisualStyleBackColor = true;
            this.bt_clearLog.Click += new System.EventHandler(this.bt_clearLog_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bt_sniff);
            this.groupBox3.Controls.Add(this.bt_logclear);
            this.groupBox3.Controls.Add(this.bt_logmem);
            this.groupBox3.Controls.Add(this.bt_show_logmode);
            this.groupBox3.Controls.Add(this.lb_logmode);
            this.groupBox3.Controls.Add(this.cb_log_mode);
            this.groupBox3.Location = new System.Drawing.Point(40, 35);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(283, 271);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log settings";
            // 
            // bt_sniff
            // 
            this.bt_sniff.Location = new System.Drawing.Point(171, 58);
            this.bt_sniff.Name = "bt_sniff";
            this.bt_sniff.Size = new System.Drawing.Size(94, 23);
            this.bt_sniff.TabIndex = 24;
            this.bt_sniff.Text = "Sniffer";
            this.toolTip_sniff.SetToolTip(this.bt_sniff, "Activate sniffer mode and uses configured logmode");
            this.bt_sniff.UseVisualStyleBackColor = true;
            this.bt_sniff.Click += new System.EventHandler(this.bt_sniff_Click);
            // 
            // bt_logclear
            // 
            this.bt_logclear.Location = new System.Drawing.Point(6, 87);
            this.bt_logclear.Name = "bt_logclear";
            this.bt_logclear.Size = new System.Drawing.Size(83, 23);
            this.bt_logclear.TabIndex = 23;
            this.bt_logclear.Text = "LOGCLEAR";
            this.bt_logclear.UseVisualStyleBackColor = true;
            this.bt_logclear.Click += new System.EventHandler(this.bt_logclear_Click);
            // 
            // bt_logmem
            // 
            this.bt_logmem.Location = new System.Drawing.Point(6, 58);
            this.bt_logmem.Name = "bt_logmem";
            this.bt_logmem.Size = new System.Drawing.Size(83, 23);
            this.bt_logmem.TabIndex = 22;
            this.bt_logmem.Text = "LOGMEM?";
            this.bt_logmem.UseVisualStyleBackColor = true;
            this.bt_logmem.Click += new System.EventHandler(this.bt_logmem_Click);
            // 
            // bt_show_logmode
            // 
            this.bt_show_logmode.Location = new System.Drawing.Point(6, 29);
            this.bt_show_logmode.Name = "bt_show_logmode";
            this.bt_show_logmode.Size = new System.Drawing.Size(83, 23);
            this.bt_show_logmode.TabIndex = 20;
            this.bt_show_logmode.Text = "LOGMODE=?";
            this.bt_show_logmode.UseVisualStyleBackColor = true;
            this.bt_show_logmode.Click += new System.EventHandler(this.bt_show_logmode_Click);
            // 
            // lb_logmode
            // 
            this.lb_logmode.AutoSize = true;
            this.lb_logmode.Location = new System.Drawing.Point(95, 34);
            this.lb_logmode.Name = "lb_logmode";
            this.lb_logmode.Size = new System.Drawing.Size(70, 13);
            this.lb_logmode.TabIndex = 19;
            this.lb_logmode.Text = "Set Logmode";
            // 
            // cb_log_mode
            // 
            this.cb_log_mode.FormattingEnabled = true;
            this.cb_log_mode.Items.AddRange(new object[] {
            "OFF",
            "MEMORY",
            "LIVE"});
            this.cb_log_mode.Location = new System.Drawing.Point(171, 31);
            this.cb_log_mode.Name = "cb_log_mode";
            this.cb_log_mode.Size = new System.Drawing.Size(94, 21);
            this.cb_log_mode.TabIndex = 18;
            this.cb_log_mode.SelectedIndexChanged += new System.EventHandler(this.cb_log_mode_SelectedIndexChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gb_serial);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(824, 333);
            this.tabPage5.TabIndex = 6;
            this.tabPage5.Text = "Serial";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gb_serial
            // 
            this.gb_serial.Controls.Add(this.groupBox5);
            this.gb_serial.Controls.Add(this.bt_Clear);
            this.gb_serial.Controls.Add(this.bt_SerialCmd);
            this.gb_serial.Controls.Add(this.tx_SerialOutput);
            this.gb_serial.Controls.Add(this.tx_serialCmd);
            this.gb_serial.Location = new System.Drawing.Point(18, 28);
            this.gb_serial.Name = "gb_serial";
            this.gb_serial.Size = new System.Drawing.Size(777, 272);
            this.gb_serial.TabIndex = 0;
            this.gb_serial.TabStop = false;
            this.gb_serial.Text = "Serial Interface";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tx_SerialHelp);
            this.groupBox5.Location = new System.Drawing.Point(615, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(133, 232);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Available Commands";
            // 
            // tx_SerialHelp
            // 
            this.tx_SerialHelp.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tx_SerialHelp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tx_SerialHelp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tx_SerialHelp.Location = new System.Drawing.Point(6, 19);
            this.tx_SerialHelp.Multiline = true;
            this.tx_SerialHelp.Name = "tx_SerialHelp";
            this.tx_SerialHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tx_SerialHelp.Size = new System.Drawing.Size(121, 207);
            this.tx_SerialHelp.TabIndex = 8;
            // 
            // bt_Clear
            // 
            this.bt_Clear.Location = new System.Drawing.Point(522, 34);
            this.bt_Clear.Name = "bt_Clear";
            this.bt_Clear.Size = new System.Drawing.Size(75, 20);
            this.bt_Clear.TabIndex = 3;
            this.bt_Clear.Text = "Clear";
            this.bt_Clear.UseVisualStyleBackColor = true;
            this.bt_Clear.Click += new System.EventHandler(this.bt_Clear_Click);
            // 
            // bt_SerialCmd
            // 
            this.bt_SerialCmd.Location = new System.Drawing.Point(259, 28);
            this.bt_SerialCmd.Name = "bt_SerialCmd";
            this.bt_SerialCmd.Size = new System.Drawing.Size(75, 20);
            this.bt_SerialCmd.TabIndex = 2;
            this.bt_SerialCmd.Text = "Send";
            this.bt_SerialCmd.UseVisualStyleBackColor = true;
            this.bt_SerialCmd.Click += new System.EventHandler(this.bt_SerialCmd_Click);
            // 
            // tx_SerialOutput
            // 
            this.tx_SerialOutput.Location = new System.Drawing.Point(22, 63);
            this.tx_SerialOutput.Multiline = true;
            this.tx_SerialOutput.Name = "tx_SerialOutput";
            this.tx_SerialOutput.ReadOnly = true;
            this.tx_SerialOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tx_SerialOutput.Size = new System.Drawing.Size(575, 188);
            this.tx_SerialOutput.TabIndex = 1;
            this.tx_SerialOutput.TextChanged += new System.EventHandler(this.tx_SerialOutput_TextChanged);
            // 
            // tx_serialCmd
            // 
            this.tx_serialCmd.Location = new System.Drawing.Point(22, 29);
            this.tx_serialCmd.Name = "tx_serialCmd";
            this.tx_serialCmd.Size = new System.Drawing.Size(231, 20);
            this.tx_serialCmd.TabIndex = 0;
            this.tx_serialCmd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_serialCmd_KeyPress);
            // 
            // gb_charging
            // 
            this.gb_charging.Controls.Add(this.tx_chrg);
            this.gb_charging.Location = new System.Drawing.Point(528, 108);
            this.gb_charging.Name = "gb_charging";
            this.gb_charging.Size = new System.Drawing.Size(103, 79);
            this.gb_charging.TabIndex = 10;
            this.gb_charging.TabStop = false;
            this.gb_charging.Text = "Charging";
            // 
            // tx_chrg
            // 
            this.tx_chrg.Location = new System.Drawing.Point(21, 32);
            this.tx_chrg.Name = "tx_chrg";
            this.tx_chrg.ReadOnly = true;
            this.tx_chrg.Size = new System.Drawing.Size(62, 20);
            this.tx_chrg.TabIndex = 1;
            this.tx_chrg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tx_thres);
            this.groupBox2.Location = new System.Drawing.Point(649, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(105, 79);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Threshold";
            // 
            // tx_thres
            // 
            this.tx_thres.Location = new System.Drawing.Point(16, 32);
            this.tx_thres.Name = "tx_thres";
            this.tx_thres.ReadOnly = true;
            this.tx_thres.Size = new System.Drawing.Size(77, 20);
            this.tx_thres.TabIndex = 2;
            this.tx_thres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // toolTip_reset
            // 
            this.toolTip_reset.ToolTipTitle = "NOTE:";
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chameleonMiniGUIToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1081, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // chameleonMiniGUIToolStripMenuItem
            // 
            this.chameleonMiniGUIToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.chameleonMiniGUIToolStripMenuItem.Name = "chameleonMiniGUIToolStripMenuItem";
            this.chameleonMiniGUIToolStripMenuItem.Size = new System.Drawing.Size(174, 20);
            this.chameleonMiniGUIToolStripMenuItem.Text = "ChameleonMiniGUI-reloaded";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolTip_link_kaos
            // 
            this.toolTip_link_kaos.ToolTipTitle = "Link";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ChameleonMiniGUI.Properties.Resources.chamRevG2;
            this.pictureBox2.Location = new System.Drawing.Point(846, 214);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(223, 333);
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            this.toolTip_link_kaos.SetToolTip(this.pictureBox2, "visit official shop KAOS (Kasper & Oswald GmbH)\r\nto buy an original ChameleonMini" +
        " RevG");
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(855, 198);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(206, 13);
            this.linkLabel1.TabIndex = 14;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Buy an original ChameleonMini from KAOS";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1081, 570);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gb_charging);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.gb_connectionSettings);
            this.Controls.Add(this.gb_rssi);
            this.Controls.Add(this.gb_output);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frm_main";
            this.Text = "Device disconnected";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Frm_main_FormClosed);
            this.Load += new System.EventHandler(this.Frm_main_Load);
            this.gb_output.ResumeLayout(false);
            this.gb_output.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gb_bootloader.ResumeLayout(false);
            this.gb_bootloader.PerformLayout();
            this.gb_connectionSettings.ResumeLayout(false);
            this.gb_connectionSettings.PerformLayout();
            this.gb_rssi.ResumeLayout(false);
            this.gb_rssi.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.gb_tools.ResumeLayout(false);
            this.gb_tools.PerformLayout();
            this.gb_newConf.ResumeLayout(false);
            this.gb_newConf.PerformLayout();
            this.gb_SlotControl.ResumeLayout(false);
            this.gb_SlotControl.PerformLayout();
            this.gb_SlotData.ResumeLayout(false);
            this.gb_SlotData.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.gb_serial.ResumeLayout(false);
            this.gb_serial.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.gb_charging.ResumeLayout(false);
            this.gb_charging.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tx_output;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox gb_output;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox gb_rssi;
        private System.Windows.Forms.Button bt_rssirefresh;
        private System.Windows.Forms.TextBox tx_rssi;
        private System.Windows.Forms.Label lb_rssi;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox gb_SlotData;
        private System.Windows.Forms.TextBox tx_size;
        private System.Windows.Forms.Label lb_size;
        private System.Windows.Forms.TextBox tx_uid;
        private System.Windows.Forms.Label lb_uid;
        private System.Windows.Forms.Label lb_config;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.GroupBox gb_bootloader;
        private System.Windows.Forms.Label lb_bootloader;
        private System.Windows.Forms.Button bt_bootloader;
        private System.Windows.Forms.Button bt_reset;
        private System.Windows.Forms.Label lb_reset;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.GroupBox gb_connectionSettings;
        private System.Windows.Forms.TextBox tx_constatus;
        private System.Windows.Forms.Button bt_disconnect;
        private System.Windows.Forms.Button bt_connect;
        private System.Windows.Forms.TextBox tx_firmware;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox gb_SlotControl;
        private System.Windows.Forms.Label lb_slot;
        private System.Windows.Forms.ComboBox cb_slots;
        private System.Windows.Forms.Button bt_clear_slot;
        private System.Windows.Forms.TextBox tx_conf_show;
        private System.Windows.Forms.Button bt_write_setting;
        private System.Windows.Forms.Button bt_read_setting;
        private System.Windows.Forms.Button bt_change_uid;
        private System.Windows.Forms.GroupBox gb_newConf;
        private System.Windows.Forms.GroupBox gb_tools;
        private System.Windows.Forms.Label lb_setConfig;
        private System.Windows.Forms.ComboBox cb_config_new;
        private System.Windows.Forms.ComboBox cb_led_red;
        private System.Windows.Forms.ComboBox cb_led_green;
        private System.Windows.Forms.ComboBox cb_l_btn_long;
        private System.Windows.Forms.ComboBox cb_r_btn_long;
        private System.Windows.Forms.ComboBox cb_l_btn;
        private System.Windows.Forms.ComboBox cb_r_btn;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tx_identify;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button bt_read_uid;
        private System.Windows.Forms.Button bt_ultralight_read;
        private System.Windows.Forms.Button bt_identify;
        private System.Windows.Forms.Button bt_clear_identify_log;
        private System.Windows.Forms.Label lb_lBtnL;
        private System.Windows.Forms.Label lb_rBtnL;
        private System.Windows.Forms.Label lb_lBtn;
        private System.Windows.Forms.Label lb_rBtn;
        private System.Windows.Forms.Label lb_ledRed;
        private System.Windows.Forms.Label lb_ledGreen;
        private System.Windows.Forms.GroupBox gb_charging;
        private System.Windows.Forms.TextBox tx_chrg;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bt_refresh;
        private System.Windows.Forms.ToolTip toolTip_refresh;
        private System.Windows.Forms.ToolTip toolTip_reset;
        private System.Windows.Forms.Label lb_warn;
        private System.Windows.Forms.Button bt_clone;
        private System.Windows.Forms.ToolTip toolTip_clone;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lb_toolText;
        private System.Windows.Forms.ToolTip toolTip_rssi;
        private System.Windows.Forms.ToolTip toolTip_chnguid;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox tx_thres;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lb_logmode;
        private System.Windows.Forms.ComboBox cb_log_mode;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem chameleonMiniGUIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TextBox tx_log;
        private System.Windows.Forms.Button bt_show_logmode;
        private System.Windows.Forms.Button bt_clearLog;
        private System.Windows.Forms.Button bt_logmem;
        private System.Windows.Forms.Button bt_logclear;
        private System.Windows.Forms.ToolTip toolTip_link_kaos;
        private System.Windows.Forms.Label lb_warn2;
        private System.Windows.Forms.Button bt_sniff;
        private System.Windows.Forms.ToolTip toolTip_sniff;
        private System.Windows.Forms.Button bt_download;
        private System.Windows.Forms.ToolTip toolTip_download;
        private System.Windows.Forms.Button bt_upload;
        private System.Windows.Forms.ToolTip toolTip_upload;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button bt_keycalc;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox gb_serial;
        private System.Windows.Forms.Button bt_Clear;
        private System.Windows.Forms.Button bt_SerialCmd;
        private System.Windows.Forms.TextBox tx_SerialOutput;
        private System.Windows.Forms.TextBox tx_serialCmd;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tx_SerialHelp;
    }
}
